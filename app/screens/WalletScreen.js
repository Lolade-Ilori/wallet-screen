import React from 'react'
import { Image, Button, SafeAreaView, StyleSheet, Text, View, StatusBar, Alert, TouchableOpacity, ScrollView, } from 'react-native'
import { AntDesign } from '@expo/vector-icons';

const WalletScreen = () => {
    return (
        <SafeAreaView style={styles.walletContainer}>
            <ScrollView style={styles.scroll}>
                <View style={styles.headerContainer}>
                    <Text style={{fontSize: 20, fontWeight: 'bold', fontFamily: 'Inter_900Black'}}>My Wallet</Text>

                    <View style={{flexDirection: 'row', alignItems: 'center',  }}>
                        <AntDesign name="qrcode" size={24} color="#5C7AEA" style={{marginRight: 20}} />
                        <Image resizeMode='cover' style={styles.profileImage} source={require('../assets/images/profile-picture.jpg')}/>
                    </View>

                </View>

                <View style={styles.balanceContainer}>
                    <Text style={{fontSize: 18, color: 'grey'}}>Balance {"\n"} </Text>
                    <Text style={{fontSize: 40}}>$22,180.00</Text>
                </View>

                <View style={styles.ctaBtnContainer}>
                    <TouchableOpacity style={styles.ctaBtn} title='Add' onPress={ ()=> Alert.alert()}>
                        <Text style={styles.ctaText}>Add</Text>  
                    </TouchableOpacity>
                    
                    <TouchableOpacity style={styles.ctaBtn} title='Add' onPress={ ()=> Alert.alert()}>
                        <Text style={styles.ctaText}>Withdraw</Text>  
                    </TouchableOpacity>
                </View>

                <View style={styles.accountInfoContainer}>
                    <Text style={{fontSize: 20, color: '#979dac'}}>Bank Accounts</Text>
                    
                    <View style={styles.detailsContainer}>
                        <View style={styles.detailsSubContainer}>
                            <Image style={styles.logo} source={require('../assets/logos/citibank.png')}/>

                            <View style={{flex: 4, marginLeft: 10}}>
                                <Text style={{fontSize: 20}}>Checking</Text>
                                <Text style={{fontSize: 18, lineHeight: 50, color: '#e0e1dd', fontWeight: '500'}}>..3421</Text>
                            </View>
                            
                            <View>
                                <Text style={{flex: 2, fontSize: 20, color: 'dodgerblue',}}>$3,800.00</Text>
                            </View>
                        </View>

                        <View style={styles.detailsSubContainer}>
                            <Image style={styles.logo} source={require('../assets/logos/citibank.png')}/>

                            <View style={{flex: 4, marginLeft: 10}}>
                                <Text style={{fontSize: 20}}>Savings</Text>
                                <Text style={{fontSize: 18, lineHeight: 50, color: '#e0e1dd', fontWeight: '500'}}>..3421</Text>
                            </View>
                            
                            <View>
                                <Text style={{flex: 2, fontSize: 20, color: '#e0e1dd',}}>Manual</Text>
                            </View>
                        </View>
                    </View>


                    <Text style={{fontSize: 20, color: '#979dac'}}>Cards</Text>
                    
                    <View style={styles.detailsContainer}>
                        <View style={styles.detailsSubContainer}>
                            <Image resizeMode='contain' style={styles.logo} source={require('../assets/logos/mastercard.png')}/>

                            <View style={{flex: 4, marginLeft: 10}}>
                                <Text style={{fontSize: 20}}>MasterCard</Text>
                                <Text style={{fontSize: 18, lineHeight: 50, color: '#e0e1dd', fontWeight: '500'}}>..8001</Text>
                            </View>
                            
                            <View>
                                <Text style={{flex: 2, fontSize: 20, color: 'dodgerblue',}}>$4,100.00</Text>
                            </View>
                        </View>

                        <View style={styles.detailsSubContainer}>
                            <Image style={styles.logo} source={require('../assets/logos/visacard.jpg')}/>

                            <View style={{flex: 4, marginLeft: 10}}>
                                <Text style={{fontSize: 20}}>Visa Premium</Text>
                                <Text style={{fontSize: 18, lineHeight: 50, color: '#e0e1dd', fontWeight: '500'}}>..5923</Text>
                            </View>
                            
                            <View>
                                <Text style={{flex: 2, fontSize: 20, color: 'dodgerblue',}}>$2,390.00</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>

        </SafeAreaView>
    )
}

export default WalletScreen

const styles = StyleSheet.create({
    accountInfoContainer: {
        borderTopColor: '#7d8597',
        borderBottomColor: '#7d8597',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        marginTop: 20,
        paddingLeft: 40,
        paddingRight: 40,
        paddingTop: 20,
        paddingBottom: 20,
        marginBottom: 20,
    },

    balanceContainer: {
        alignItems: 'center',
        paddingLeft: 40,
        paddingRight: 40,
        paddingTop: 20,
        paddingBottom: 20,
    },

    ctaBtn: {
        borderRadius: 30,
        backgroundColor: '#0466c8',
        padding: 15,
        width: '45%',
    },

    ctaBtnContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        justifyContent: 'space-around',
        paddingLeft: 40,
        paddingRight: 40,
        paddingTop: 20,
        paddingBottom: 20,  

    },

    ctaText: {
        color: '#fff',
        borderColor: '#fff',
        textAlign: 'center',
        fontSize: 18,
    },

    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 40,
        paddingRight: 40,
        paddingTop: 20,
        paddingBottom: 20,
        alignItems: 'center',
    },

    detailsContainer: {
        marginTop: 30,
    },

    detailsSubContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 20,
    },

    logo: {
        width: 60,
        height: 60,
        flex: 1,
    },

    profileImage: {
        width: 50,
        height: 50,
        borderRadius: 50,
    }, 

    scroll: {
        flexGrow: 1,
    },

    walletContainer: {
        flexGrow: 1,
        backgroundColor: '#fff',
        overflow: 'scroll',
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0
    },
})

